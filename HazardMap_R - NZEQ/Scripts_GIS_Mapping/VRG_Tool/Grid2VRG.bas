Attribute VB_Name = "Grid2VRG"
Option Compare Database
'Coded by Furkan A

Public Function RunGrid2VRG()
   Grid2VRG
End Function

'Dim tbl As Recordset

Public Sub Grid2VRG()

Const Maxgrdpt = 2000000
Const MinDist0 = 0.5
Const p = 2#


Dim db As Database

Dim ID As Double
Dim vrg As Double
Dim Vlong As Double
Dim Vlat As Double
Dim Glong(Maxgrdpt) As Double
Dim Glat(Maxgrdpt) As Double
Dim zip As Double
Dim pga(Maxgrdpt) As Double
Dim pgv(Maxgrdpt) As Double
Dim mmi(Maxgrdpt) As Double
Dim pga03(Maxgrdpt) As Double
Dim pga10(Maxgrdpt) As Double
Dim pga30(Maxgrdpt) As Double
Dim wtpga As Double
Dim wtpgv As Double
Dim wtmmi As Double
Dim wtpga03 As Double
Dim wtpga10 As Double
Dim wtpga30 As Double
Dim dist As Double
Dim wtdist As Double
Dim MaxDist As Double

Dim MinDist As Double
Dim delta As Double
Dim MaxLong As Double
Dim MinLong As Double
Dim MinLat As Double
Dim MaxLat As Double
Dim Min_k As Long
Dim level As Integer

Dim count As Long
Dim totcount As Long
Dim totcnt As Long
Dim rec As Long
Dim i As Long


Dim sumwt As Double
rec = 0
count = 0
MaxDist = 5
MinDist = MinDist0

Call DoCmd.SetWarnings(WarningsOff)
Call DoCmd.CopyObject(, "Grid2VRG", acTable, "tmp_Grid2VRG")

Set db = CurrentDb()
Set recsetVRG = db.OpenRecordset("VRG")
Set recsetGrid = db.OpenRecordset("Grid")
Set OutTbl = db.OpenRecordset("Grid2VRG", dbOpenTable)

recsetVRG.MoveLast
totcount = recsetVRG.RecordCount
recsetGrid.MoveLast
totcnt = recsetGrid.RecordCount

'*****************************************************************
 varReturn = SysCmd(acSysCmdInitMeter, "Reading Grid file :", 100)
 '****************************************************************

'----------------------------------------------------
'    READ GRID DATA INTO MEMORY
'----------------------------------------------------
i = 1
With recsetGrid
    .MoveFirst
    Do Until .EOF
        Glong(i) = !Long
        Glat(i) = !Lat
        pga(i) = !pga
        pgv(i) = !pgv
        mmi(i) = !mmi
        pga03(i) = !pga03
        pga10(i) = !pga10
        pga30(i) = !pga30
        i = i + 1
        '******************************************************************
        varReturn = SysCmd(acSysCmdUpdateMeter, Int(i / totcnt * 100))
        '*******************************************************************
        .MoveNext
    Loop
End With
'totcnt = i
recsetGrid.Close

'----------------------------------------------------
'    START OF MAIN LOOP
'----------------------------------------------------


With recsetVRG
   .MoveFirst
    Do Until .EOF
        rec = rec + 1
        vrg = !GeoID
        zip = !zip
        Vlong = !Long
        Vlat = !Lat
 
        '**********************************************************************
        varReturn = SysCmd(acSysCmdInitMeter, "Processing VRG: " & vrg, 100)
        varReturn = SysCmd(acSysCmdUpdateMeter, Int(rec / totcount * 100))
        '**********************************************************************
 
        level = 2
        MaxDist = 5#
Firstgrid:
        '--------------------------------------------------------
        '  LOOP FOR ALL GRID DATA
        '--------------------------------------------------------
        count = 0
        '----  INITIALIZE COUNTERS ---
        sumwt = 0#
        wtpga = 0#
        wtpgv = 0#
        wtmmi = 0#
        wtpga03 = 0#
        wtpga10 = 0#
        wtpga30 = 0#
            
        For k = 1 To i
        
            '----  SET LONG/LAT BOUNDARIES ---
            delta = MaxDist / 100
            MaxLong = Vlong + delta
            MinLong = Vlong - delta
            MaxLat = Vlat + delta
            MinLat = Vlat - delta
            
            If (Glong(k) > MinLong And Glong(k) < MaxLong) Then
                
                If (Glat(k) > MinLat And Glat(k) < MaxLat) Then
                
                    dist = calcdist(Vlong, Vlat, Glong(k), Glat(k))
                        
                    If (dist < MinDist) Then
                        MinDist = dist
                        Min_k = k
                        level = 1
                        
                    ElseIf (level = 2) Then
                    
                        wtdist = (1 / dist) ^ p
                        sumwt = sumwt + wtdist
                        
                        wtpga = wtpga + wtdist * pga(k)
                        wtpgv = wtpgv + wtdist * pgv(k)
                        wtmmi = wtmmi + wtdist * mmi(k)
                        wtpga03 = wtpga03 + wtdist * pga03(k)
                        wtpga10 = wtpga10 + wtdist * pga10(k)
                        wtpga30 = wtpga30 + wtdist * pga30(k)
                        
                        count = count + 1
                        
                    End If
                End If
            End If
        Next k
        
        If (level = 2 And count < 3) Then
            MaxDist = MaxDist + 5
            
            If (MaxDist > 45) Then
                GoTo Nextvrg
            Else
                GoTo Firstgrid
            End If
         End If
         
        '--------------------------------------------------------
        '  WRITE WEIGHTED HAZARD TO OUTPUT TABLE
        '--------------------------------------------------------
        OutTbl.AddNew
        OutTbl!vrg = vrg
        OutTbl!zip = zip
        'OutTbl!EVENTID = "1906001"
        If (level = 1) Then
            OutTbl!wtpga = pga(Min_k) / 100
            OutTbl!wtpgv = pgv(Min_k)
            OutTbl!wtmmi = mmi(Min_k)
            OutTbl!PSA03 = pga03(Min_k) / 100
            OutTbl!PSA10 = pga10(Min_k) / 100
            OutTbl!PSA30 = pga30(Min_k) / 100
        
        Else
            OutTbl!wtpga = wtpga / (sumwt * 100)
            OutTbl!wtpgv = wtpgv / sumwt
            OutTbl!wtmmi = wtmmi / sumwt
            OutTbl!PSA03 = wtpga03 / (sumwt * 100)
            OutTbl!PSA10 = wtpga10 / (sumwt * 100)
            OutTbl!PSA30 = wtpga30 / (sumwt * 100)
        End If
        OutTbl.Update
        
Nextvrg:

    .MoveNext
    Loop
End With

recsetVRG.Close

OutTbl.Close
varReturn = SysCmd(acSysCmdClearStatus)

End Sub
        
Function calcdist(x1 As Double, y1 As Double, x2 As Double, y2 As Double)

Dim RdVLat As Double
Dim RdVLong As Double
Dim RdGLat As Double
Dim RdGLong As Double

RdVLong = (x1 * 3.14592654) / 180
RdVLat = (y1 * 3.14592654) / 180
RdGLong = (x2 * 3.14592654) / 180
RdGLat = (y2 * 3.14592654) / 180

temprad = Sin(RdVLat) * Sin(RdGLat) + Cos(RdVLat) * Cos(RdGLat) * Cos(RdVLong - RdGLong)
rd = Atn(-temprad / Sqr(-temprad * temprad + 1)) + 2 * Atn(1)
calcdist = (rd * 111.12 * 180) / 3.14592654

End Function



           
