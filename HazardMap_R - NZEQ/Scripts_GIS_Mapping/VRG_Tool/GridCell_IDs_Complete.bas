Attribute VB_Name = "GridCell_IDs"
'===================================
' Access VBA version
'
'   Updated: 2008.10.14 --- Albert Chen
'       - Complement the rest 2 resolutions (Res = 8 and 9)
'       - Completed the rest 2 quadrants (Chile and Australia)
'       - Fixed the bug about generating incorrect VRGID when resolution = 6
'   Updated: 2005.09.01 --- gilbertM
'       - added half-resolutions for 2 quadrants (US and Japan)
'   First release: 2004.01.28 --- gilbertM
'       - Generates VRG for resolutions 1,3,5, and 7 only
'
'===================================
Function test_VRG()

void = VRG_GeoID(145.099761, 43.274166, 4)
void = VRG_GeoID(-79.031, 33.572, 4)

End Function

Function VRG_GeoID(lon, lat, res)
Attribute VRG_GeoID.VB_ProcData.VB_Invoke_Func = " \n14"
' Variable Resolution Grid ID generator
' Res = Resolution type:
'     = 1  (1.0 deg)
'     = 2  (0.5 deg)
'     = 3  (0.1 deg)
'     = 4  (0.05 deg)
'     = 5  (0.01 deg)
'     = 6  (0.005 deg)
'     = 7  (0.001 deg)
'     = 8  (0.0005 deg)
'     = 9  (0.0001 deg)
'
' by: GilbertM 9.06.01
'-----------------------------------------------------------------
Dim rr As Integer, Q As Integer

rr = 0
'--- DETERMINE THE QUADRANT OF THE LOCATION ---
If (lon >= 0) Then
    If (lat >= 0) Then
        Q = 1
    Else
        Q = 7
    End If
Else
    If (lat >= 0) Then
        Q = 3
    Else
        Q = 5
    End If
End If
rr = Q * 100

'--- MODIFIED BEC. ACCESS FORMAT() ROUNDS VALUES INSTEAD OF TRUNCATING ---
lat_5 = Format(Abs(lat * 1000000), "00000000")  'Modified 1/28/04 GLM
lon_6 = Format(Abs(lon * 1000000), "000000000") 'Modified 1/28/04 GLM

lat_5 = Mid(lat_5, 1, 6)
lon_6 = Mid(lon_6, 1, 7)

rr = rr + Int(Abs(lon))
vrg = Format(rr, 0)
vrg = vrg + Mid(lat_5, 1, 2)

'--- Added functionalities for Res = 8 and 9, and re-write the structure
For i = 3 To 6
    vrg = vrg + Mid(lon_6, i + 1, 1) + Mid(lat_5, i, 1)
Next i

If (res < 9) Then
    If (res Mod 2 = 1) Then
        vrg = Mid(vrg, 1, res + 4)
    Else
        vrg = Mid(vrg, 1, res + 3) + last_VRG(Val(Mid(vrg, res + 4, 1)), Val(Mid(vrg, res + 5, 1)), Q)
    End If
End If

VRG_GeoID = Val(vrg)

End Function

Function last_VRG(last_X As Integer, last_Y As Integer, Q As Integer) As String
'-----------------------------------------------------------------
' Q = global quadrant of the VRG cell as defined by the SPEC
' WARNING!  This function is current defined for Q =  in (+) long / (+) lat domain only, for now!
'
' by: GilbertM 8.31.2005
'-----------------------------------------------------------------

    Select Case Q
    '--- NORTH & WEST HEMISPHERES (JP, TW, etc.)
    Case 1
        If last_X < 5 Then
            If last_Y < 5 Then
                last_VRG = 5
            Else
                last_VRG = 3
            End If
        Else
            If last_Y < 5 Then
                last_VRG = 7
            Else
                last_VRG = 1
            End If
        End If
    '--- NORTH & EAST HEMISPHERES (US)
    Case 3
        If last_X < 5 Then
            If last_Y < 5 Then
                last_VRG = 7
            Else
                last_VRG = 1
            End If
        Else
            If last_Y < 5 Then
                last_VRG = 5
            Else
                last_VRG = 3
            End If
        End If
    '--- SOUTH & EAST HEMISPHERES (Chile, Peru, etc.)
    Case 5
        If last_X < 5 Then
            If last_Y < 5 Then
                last_VRG = 1
            Else
                last_VRG = 7
            End If
        Else
            If last_Y < 5 Then
                last_VRG = 3
            Else
                last_VRG = 5
            End If
        End If
    '--- SOUTH & WEST HEMISPHERES (AU, NZ, etc.)
    Case 7
        If last_X < 5 Then
            If last_Y < 5 Then
                last_VRG = 3
            Else
                last_VRG = 5
            End If
        Else
            If last_Y < 5 Then
                last_VRG = 1
            Else
                last_VRG = 7
            End If
        End If
        
    End Select
    
End Function
