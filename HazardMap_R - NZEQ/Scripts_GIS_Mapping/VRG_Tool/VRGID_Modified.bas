Attribute VB_Name = "VRGID_Modified"
'================================================================================================
'Convert VRG into Lat, Long
'
'   Updated: 2009.07.22 --- Albert Chen
'       - Change names of variables from quard, and subquard to quad, and subquad
'       - Fix the bug that some invalid VRGIDs will still return lat/long
'
'   Updated: 2008.10.13 --- Albert Chen
'       - Fixed the bug that VRGIDs have 1)RRR = 100, 300, 500 and 700, or 2) YY = 00
'         can't be converted back to lat and long.
'       - Completed the implementation so that the tool can apply to every Quadrant and resolution
'
'================================================================================================
Option Explicit
Public Function GetLongitude(vrgID As String)
'Translated VRGID into Longitude
Dim fChar As Integer, quad As Integer
Dim intPart As Integer, fracChar As Double, l As Integer, signPart As Integer
quad = Getquad(vrgID)
Select Case quad
  Case 100
    intPart = (Left(vrgID, 3) - quad)
    signPart = 1    'Added 10/13/2008 AC
Case 300
    intPart = (Left(vrgID, 3) - quad)
    signPart = -1   'Added 10/13/2008 AC
Case 500
    intPart = (Left(vrgID, 3) - quad)
    signPart = -1   'Added 10/13/2008 AC
Case 700
    intPart = (Left(vrgID, 3) - quad)
    signPart = 1    'Added 10/13/2008 AC
End Select

l = Len(vrgID)
Select Case l
Case 6
  fracChar = TranslateSubquad(quad / 100, Val(Right(vrgID, 1)), 1) / 10
Case 7
  fracChar = Val(Mid(vrgID, 6, 1)) / 10
Case 8
  fracChar = (Val(Mid(vrgID, 6, 1)) / 10) + TranslateSubquad(quad / 100, Val(Right(vrgID, 1)), 1) / 100
Case 9
  fracChar = Val(Mid(vrgID, 6, 1) & Mid(vrgID, 8, 1)) / 100
Case 10
  fracChar = Val(Mid(vrgID, 6, 1) & Mid(vrgID, 8, 1)) / 100 + TranslateSubquad(quad / 100, Val(Right(vrgID, 1)), 1) / 1000
Case 11
  fracChar = Val(Mid(vrgID, 6, 1) & Mid(vrgID, 8, 1) & Mid(vrgID, 10, 1)) / 1000
Case 12 'Added 10/22/2008 AC
  fracChar = Val(Mid(vrgID, 6, 1) & Mid(vrgID, 8, 1) & Mid(vrgID, 10, 1)) / 1000 + TranslateSubquad(quad / 100, Val(Right(vrgID, 1)), 1) / 10000
Case 13 'Added 10/22/2008 AC
  fracChar = Val(Mid(vrgID, 6, 1) & Mid(vrgID, 8, 1) & Mid(vrgID, 10, 1) & Mid(vrgID, 12, 1)) / 10000
Case Else
End Select

If signPart = -1 And (Abs(intPart) + fracChar) = 0 Then
    GetLongitude = "This is an invalid VRGID"
Else
    GetLongitude = signPart * (Abs(intPart) + fracChar) 'Modified 10/13/2008 AC
End If
End Function
Public Function GetLatitude(vrgID As String)
'Translated VRGID into Latitude
Dim fChar As Integer, quad As Integer
Dim intPart As Integer, fracChar As Double, l As Integer, signPart As Integer
quad = Getquad(vrgID)
Select Case quad
  Case 100
    intPart = Mid(vrgID, 4, 2)
    signPart = 1    'Added 10/13/2008 AC
Case 300
    intPart = Mid(vrgID, 4, 2)
    signPart = 1    'Added 10/13/2008 AC
Case 500
    intPart = Val(Mid(vrgID, 4, 2))
    signPart = -1   'Added 10/13/2008 AC
Case 700
    intPart = Val(Mid(vrgID, 4, 2))
    signPart = -1   'Added 10/13/2008 AC
End Select

l = Len(vrgID)
Select Case l
Case 6
  fracChar = TranslateSubquad(quad / 100, Val(Right(vrgID, 1)), 2) / 10
Case 7
  fracChar = Val(Mid(vrgID, 7, 1)) / 10
Case 8
  fracChar = (Val(Mid(vrgID, 7, 1)) / 10) + TranslateSubquad(quad / 100, Val(Right(vrgID, 1)), 2) / 100
Case 9
  fracChar = Val(Mid(vrgID, 7, 1) & Mid(vrgID, 9, 1)) / 100
Case 10
  fracChar = Val(Mid(vrgID, 7, 1) & Mid(vrgID, 9, 1)) / 100 + TranslateSubquad(quad / 100, Val(Right(vrgID, 1)), 2) / 1000
Case 11
  fracChar = Val(Mid(vrgID, 7, 1) & Mid(vrgID, 9, 1) & Mid(vrgID, 11, 1)) / 1000
Case 12 'Added 10/22/2008 AC
  fracChar = Val(Mid(vrgID, 7, 1) & Mid(vrgID, 9, 1) & Mid(vrgID, 11, 1)) / 1000 + TranslateSubquad(quad / 100, Val(Right(vrgID, 1)), 2) / 10000
Case 13 'Added 10/22/2008 AC
  fracChar = Val(Mid(vrgID, 7, 1) & Mid(vrgID, 9, 1) & Mid(vrgID, 11, 1) & Mid(vrgID, 13, 1)) / 10000
Case Else
End Select

If signPart = -1 And (Abs(intPart) + fracChar) = 0 Then
    GetLatitude = "This is an invalid VRGID"
Else
    GetLatitude = signPart * (Abs(intPart) + fracChar)  'Modified 10/13/2008 AC
End If

End Function
Public Function Getquad(vrgID As String) As Integer
'Got Quadrant from VRGID
Dim quad As Integer, fChar As Integer
fChar = Val(Left(vrgID, 1)) * 100
Select Case fChar
Case 100
  quad = 100
Case 200
  quad = 100
Case 300
  quad = 300
Case 400
  quad = 300
Case 500
  quad = 500
Case 600
  quad = 500
Case 700
  quad = 700
Case 800
  quad = 700
End Select
Getquad = quad
End Function
Public Function TranslateSubquad(thisQuad, thisSubQuad, co_type) As Integer
'Approximated the last digit of Latitude and Longitude by judging sub-quadrants when resolution is even numbers
'Modified 10/15/2008 AC
Dim intArrow As Integer
Select Case thisQuad
Case 1
  intArrow = Choose((thisSubQuad + 1) / 2 + (co_type - 1) * 4, 1, -1, -1, 1, 1, 1, -1, -1)
Case 3
  intArrow = Choose((thisSubQuad + 1) / 2 + (co_type - 1) * 4, -1, 1, 1, -1, 1, 1, -1, -1)
Case 5
  intArrow = Choose((thisSubQuad + 1) / 2 + (co_type - 1) * 4, -1, 1, 1, -1, -1, -1, 1, 1)
Case 7
  intArrow = Choose((thisSubQuad + 1) / 2 + (co_type - 1) * 4, 1, -1, -1, 1, -1, -1, 1, 1)
End Select

If intArrow > 0 Then
    TranslateSubquad = 7
Else
    TranslateSubquad = 2
End If

End Function
