!=======================================================
SUBROUTINE  MCV06_SHALLOW_SAP(Mw, eDist,  h, ftype, CC,Ci, SAp)
!=======================================================
double precision :: CC(22), C(46), Mw, eDist, h, ftype,w
integer Ci(22)
double precision,intent(OUT) :: SAp

!  ! Function MCVERRY_ETAL_06_SHALLOW_SAp(ByVal Mw, ByVal eDist, ByVal h, ftype, CC, Ci)
!  !   McVerry, Zhao, Abrahamson, and Sommerville (2006) - uses Mw
!  !   ftype: 0=Unspecified, 1=SS, 2=REV, 3=IF, 4=IS, 5=NRML, 6=TRST, 7=OBLQ
!  !   eDist = closest dist to surface projection
!  !   NEHRP Soil Class ! (Vs=760 m/s)

!  !---  REINDEX THE COEFFICIENTS BASED ON VECTOR, Ci ---
do i = 1,22 
C(Ci(i)) = CC(i)
end do

!---  SET COEFFICIENTS BASED ON FAULT TYPE ---
Select Case (int(ftype))
	case (0 ,1)   !--- Default / Strike-Slip Fault
	Cn = 0.0000
	CR = 0.0000
	Case (2)  !! Reverse Fault
	Cn = 0.0000
	CR = 1.0000
	Case (5)  !! Normal Fault
	Cn = -1.000
	CR = 0.0000
	case (7)  !! Oblique Fault
	Cn = 0.0000
	CR = 0.5000
end select  

rr = sqrt(h ** 2 + eDist ** 2)

R2 = C(5) * rr + (C(8) + C(6) * (Mw - 6)) * log((rr ** 2 + C(10) ** 2) ** 0.5)
w = C(1) + C(4) * (Mw - 6) + C(3) * (8.5 - Mw) ** 2 + R2 + C(32) * Cn + C(33) * CR
SAp = exp(w)

return
End subroutine MCV06_SHALLOW_SAP

!=======================================================
SUBROUTINE  MCV06_SHALLOW_PGA(Mw,  eDist,  h, ftype, PGA, sigma)
!=======================================================
!DEC$ ATTRIBUTES DLLEXPORT::MCV06_SHALLOW_PGA
double precision :: CC(22), Mw, eDist, h, ftype
integer Ci(22)
double precision, intent(out) :: PGA, sigma
!!    InitArray("pMcVerry_06", CC) !--- USES COEFFICIENT FOR PGA
CC = (/ 0.1427, 0.0000, -0.1440, -0.0099, 0.1700, -0.6874, 5.6000, 8.5734, 1.4140, & 
0.0000, -2.5520, -2.5659, 1.7818, 0.5540, 0.0155, -0.4996, 0.2732, -0.2300, 0.2000, 0.2600, -0.3372, -0.0326 /)
!!    InitArray("cMcVerry_06", Ci)
Ci = (/ 1,3,4,5,6,8,10,11,12,13,15,17,18,19,20,24,29,30,32,33,43,46 /)

call MCV06_SHALLOW_SAP(Mw, eDist, h, ftype, CC, Ci, PGA)
call MCV06_GeoMean_Sigma(Mw, dble(0.0), sigma)
return
end subroutine MCV06_SHALLOW_PGA

!=======================================================
SUBROUTINE  MCV06_SHALLOW_PGAP( Mw,  eDist,  h, ftype, PGAp)
!=======================================================
double precision :: CC(22), Mw, eDist, h, ftype
integer Ci(22)
double precision, intent(OUT) :: PGAp

!Call InitArray("qMcVerry_06", CC) !--- USES COEFFICIENT FOR PGAp
CC = (/0.0771,0.0000,-0.1440,-0.0090,0.1700,-0.7373,5.6000,8.0861,1.4140,0.0000, & 
-2.5520,-2.4989,1.7818,0.5540,0.0159,-0.4322,0.3873,-0.2300,0.2000,0.2600,-0.3104,-0.0325/)
!!    InitArray("cMcVerry_06", Ci)
Ci = (/ 1,3,4,5,6,8,10,11,12,13,15,17,18,19,20,24,29,30,32,33,43,46 /)

call MCV06_SHALLOW_SAP(Mw, eDist, h, ftype, CC, Ci, PGAp)
return
End subroutine MCV06_SHALLOW_PGAP

!=======================================================
SUBROUTINE  MCV06_SHALLOW_SA( Mw,  eDist,  h, ftype, T, SA, sigma)
!=======================================================
!   McVerry, Zhao, Abrahamson, and Sommerville (2006) - uses Mw
!   ftype: 0=Unspecified, 1=SS, 2=REV, 3=IF, 4=IS, 5=NRML, 6=TRST, 7=OBLQ
!   eDist = closest dist to surface projection
!   NEHRP Soil Class ! (Vs=760 m/s)
!   Eqtn Source:
!DEC$ ATTRIBUTES DLLEXPORT::MCV06_SHALLOW_SA
double precision :: CC(22), Mw, eDist, h, ftype, T, PGA, PGAp, Sap
integer :: Ci(22)
double precision :: SaRatio,factor_dist(3),factor_wts(3), wt, pgaNZ, SaNZ1, pgaUS, SaUS1
integer :: NZatten,USatten
double precision, intent(OUT):: SA, sigma
 
SaRatio = 1.0
!wt = 0.5
factor_dist = (/0.0, 25.0,  50.0/)
factor_wts = (/0.75, 0.5,  0.0/)
call LTD_INTERP(factor_dist, factor_wts, eDist, wt)

IF (flag == 1 .AND. eDist < 50) THEN
	Select Case (int(ftype))
        Case (0, 1) !STRIKE-SLIP
            NZatten = 9010
            USatten = 5013
        Case (5) !NORMAL/EXTENSIONAL
            NZatten = 9011
            USatten = 5014
        Case (2, 6) !REVERSE / THRUST
            NZatten = 9012
            USatten = 5015
        Case (7) !OBLIQUE
            NZatten = 9013
            USatten = 5015
	End Select
        
	! pgaNZ = SA_Spectra("NZ", NZatten, Mw, eDist, h, 0)
	! SaNZ1 = SA_Spectra("NZ", NZatten, Mw, eDist, h, T)
	
	! pgaUS = SA_Spectra("US", USatten, Mw, eDist, h, 0)
	! SaUS1 = SA_Spectra("US", USatten, Mw, eDist, h, T)

	! SaNorm = (SaUS1 / pgaUS) * wt + (SaNZ1 / pgaNZ) * (1 - wt)
	! MCVERRY_ETAL_06_SHALLOW_SA = pgaNZ * SaNorm
ELSE 
	IF (T ==0.3) THEN
	! 0.3 sec ONLY 
	CC = (/0.2112,-0.0360,-0.1440,-0.0103,0.1700,-0.5240,4.8000,9.2178,1.4140, & 
	-0.0036,-2.4540,-2.4736,1.7818,0.5540,0.0130,-0.5660,0.5321,-0.1950,0.2000,0.1980,-0.0757,-0.0354/)
	ELSE IF (T == 1.0) THEN
	! 1 sec ONLY
	CC = (/-0.5140,-0.1020,-0.1440,-0.0065,0.1700,-0.5867,3.7000,6.9874,1.4140, &
	-0.0064,-2.2340,-2.2826,1.7818,0.5540,0.0093,0.0201,0.3300,0.0000,0.2000,0.0130,0.8924,-0.0250/)
	ELSE IF (T == 3.0) THEN
	! 3 sec ONLY
	CC = (/-1.5657,-0.1726,-0.1440,-0.0062,0.1700,-0.5226,3.5000,5.0542,1.4140, & 
	 -0.0089,-2.0330,-2.0556,1.7818,0.5540,-0.0027,-0.2397,0.0987,0.0400,0.2000,-0.1560,0.6094,-0.0159/)
	END IF
	!!    InitArray("cMcVerry_06", Ci)
	Ci = (/ 1,3,4,5,6,8,10,11,12,13,15,17,18,19,20,24,29,30,32,33,43,46 /)

	call MCV06_SHALLOW_SAP(Mw, eDist, h, ftype, CC, Ci,SAp)
	call MCV06_SHALLOW_PGA(Mw, eDist, h, ftype, PGA, sigma)
	call MCV06_SHALLOW_PGAP(Mw, eDist, h, ftype, PGAp)
	SA = SAp * (PGA / PGAp)
END IF
call MCV06_GeoMean_Sigma(Mw, T, sigma)
end subroutine MCV06_SHALLOW_SA

!=======================================================
! Calculation Module of Uncertainty of NZEQ GMPE McV06 
SUBROUTINE  MCV06_GeoMean_Sigma(Mw, T, sigma)
!=======================================================
double precision, intent(IN) :: Mw, T
double precision :: intra,intraLB,intraUB, inter, t1,t2,weight
integer :: Num_T,UB,LB
double precision, intent(OUT) :: sigma
double precision :: Periods(13), SigmaM6(13), Sigslope(13),Tau(13),SigtotM6(13)

Periods= (/0.00,0.00,0.08,0.10,0.20,0.30,0.40,0.50,0.75,1.00,1.50,2.00,3.00/)
SigmaM6 = (/0.4871,0.5099,0.5297,0.5401,0.5599,0.5456,0.5556,0.5658,0.5611,0.5573,0.5419,0.5419,0.5809/)
Sigslope = (/-0.1011,-0.0259,-0.0703,-0.0292,0.0172,-0.0566,-0.1064,-0.1123,-0.0836,-0.0620,0.0385,0.0385,0.1403/)
Tau = (/0.2677,0.2469,0.3139,0.3017,0.2583,0.1967,0.1802,0.1440,0.1871,0.2073,0.2405,0.2405,0.2053/)
SigtotM6 =	(/0.5558,0.5666,0.6157,0.6187,0.6166,0.5800,0.5840,0.5839,0.5915,0.5946,0.5929,0.5929,0.6161/)

IF (T==0) THEN ! PGA -- -- no need to interpolate
	if ((Mw >5) .AND. (Mw < 7)) then
		intra = SigmaM6(1) + Sigslope(1) * (Mw-6)
	else if (Mw <= 5) then
		intra = SigmaM6(1) - Sigslope(1) 
	else if (Mw >= 7) then	
		intra = SigmaM6(1) + Sigslope(1)
	end if
	inter = Tau(1) 
ELSE IF (T >0) THEN !SA -- find TL and TU to interpolate
	Num_T = UBound(Periods,1)
		!--- CHECK IF THE PERIOD IS LARGER THAN MAXIMUM PERIOD DEFINED ---
	If (T >= Periods(Num_T)) Then
		LB = Num_T
		UB = LB
		t1 = Periods(Num_T)
		t2 = t1
	Else
		!--- SEARCH FOR THE UPPER & LOWER BOUNDS FOR THE PERIOD ---
		UB = 2  ! First one is for PGA 
		do while (Periods(UB) <= T)
			UB = UB + 1
		end do				
		LB = UB - 1
		t1 = Periods(LB)
		t2 = Periods(UB)
	End If
	
	weight = (T-t1)/(t2-t1)

	if ((Mw >5) .AND. (Mw < 7)) then
		intraUB = SigmaM6(UB) + Sigslope(UB) * (Mw-6)
		intraLB = SigmaM6(LB) + Sigslope(LB) * (Mw-6)
	else if (Mw <= 5) then
		intraUB = SigmaM6(UB) - Sigslope(UB) 
		intraLB = SigmaM6(LB) - Sigslope(LB)
	else if (Mw >= 7) then	
		intraUB = SigmaM6(UB) + Sigslope(UB)
		intraLB = SigmaM6(LB) + Sigslope(LB)
	end if
		
	IF (abs(Periods(LB) - T) <= 1E-5) THEN ! find exact matching period
	intra = intraLB
	inter = Tau(LB)
	ELSE
	intra = intraLB + weight*(intraUB - intraLB)
	inter = Tau(LB) + weight*(Tau(UB) - Tau(LB))
	END IF
END IF	
sigma = sqrt(intra**2 + inter**2)	
return
end subroutine MCV06_GeoMean_Sigma

!=======================================================
!Piece-Wise Linear Interpolation of sorted arrays 
!does not extrapolate, takes first or last values if "a" is out of bounds
SUBROUTINE LTD_INTERP( x,  Y,  a, b)
!=======================================================
implicit none
double precision :: x(3),Y(3),a, b
integer i
intent(out) :: b

If (a <= x(LBound(x,1))) Then
    b = Y(LBound(Y,1))
Else If (a >= x(UBound(x,1))) Then
    b = Y(UBound(Y,1))
Else
    i = 1
    do while (a > x(i))
       i = i + 1
    end do
    b = (a - x(i - 1)) * (Y(i) - Y(i - 1)) / (x(i) - x(i - 1))
    b = b + Y(i - 1)
End If
return
End subroutine LTD_INTERP