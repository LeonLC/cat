!---------------------------------------------------------------
Function Get_HazVAL(RtnPrd , ary_Ymean, ary_Yrate, ary_Count , sigma_lnY, distflag ) 
!--------------------------------------------------------------- 
!DEC$ ATTRIBUTES DLLEXPORT::dist  
                 
		DOUBLE PRECISION, parameter :: ProbRes = 0.0000001, HazRes = 0.0001
		DOUBLE PRECISION ::	RtnPrd, sigma_LnY, ary_Ymean(10000000), ary_Yrate(10000000)
		integer	:: ary_Count, distflag  !--- 1=LOGNORMAL, 2=NORMAL

	    !--- Search for corresponding hazard value using half-interval method ----
	    DOUBLE PRECISION ::	yHI, yLO, fyHI, fyLO , yMD, fyMD, Prob_err, Haz_err
    
	    if (distflag==2) then
			yHI = 12.0
			yLO = 0.00000001
		else
			yHI = 5.0
			yLO = 0.00000001
		endif

	    fyHI = SumProbExcd(yHI, ary_Ymean, ary_Yrate, ary_Count,sigma_lnY, distflag)
	    fyLO = SumProbExcd(yLO, ary_Ymean, ary_Yrate, ary_Count,sigma_lnY, distflag)
         
	    !--- CHECK FOR EXTREME VALUES -----
	    If ((1.0 / fyLO) > RtnPrd) Then
		  Get_HazVAL = yLO
    
	    ElseIf (1.0 / fyHI < RtnPrd) Then
		  Get_HazVAL = yHI
    
	    Else
		Prob_err = 100000.0
		Haz_err = 100000.0
        
		  do While ((Prob_err > ProbRes) .and. (Haz_err > HazRes))

			  yMD = (yHI + yLO) / 2.0
			  fyMD = SumProbExcd(yMD, ary_Ymean, ary_Yrate, ary_Count,sigma_lnY, distflag)            

			  Prob_err = Abs(fyMD - (1.0 / RtnPrd))
			  Haz_err = yHi - yLo
            
			  If (fyMD > (1.0 / RtnPrd)) Then
				  yLO = yMD
				  fyLO = fyMD
			  Else
				  yHI = yMD
				  fyHI = fyMD
			  End If
		  enddo
        
		  Get_HazVAL = yMD
	    End If

End Function Get_HazVAL

!---------------------------------------------------------------
Function SumProbExcd(Ycrit, ary_Ymean, ary_Yrate,ary_Count, sigma_lnY, distflag)
!---------------------------------------------------------------

		INTEGER :: ary_Count, r, distflag  !--- 1=LOGNORMAL, 2=NORMAL
		DOUBLE PRECISION ::	ary_Ymean(10000000), ary_Yrate(10000000), Ycrit, sigma_lnY, sumprob

		sumprob = 0.0
		do r = 1, ary_Count
		  sumprob = sumprob + ary_Yrate(r) * ProbExcd(Ycrit, ary_Ymean(r), sigma_lnY, distflag)
		enddo
		SumProbExcd = sumprob

End Function SumProbExcd

!---------------------------------------------------------------
Function ProbExcd(Ycrit, Ymean, sigma, distflag)
!---------------------------------------------------------------
 
		DOUBLE PRECISION :: Ycrit, Ymean, sigma, Zvar
		integer distflag !--- 1=LOGNORMAL, 2=NORMAL

!	Zvar = log(Ycrit)
		
		select case (distflag)
		
			case (1)  !--- LOG-NORMAL DISTRIBUTION ASSUMPTION
				Zvar = (log(Ycrit) - log(Ymean)) / sigma
			
			case (2)  !--- NORMAL DISTRIBUTION ASSUMPTION
				Zvar = (Ycrit - Ymean) / sigma

		end select	

		ProbExcd = 1.0 - StdNorm_CDF(Zvar)

End Function ProbExcd

!---------------------------------------------------------------
Function Factorial(N)
!---------------------------------------------------------------
		integer :: N, i, Fact
		DOUBLE PRECISION Factorial
	    Fact = 1
	    do i = 1 , N
		  Fact = Fact * i
	    enddo 
	    Factorial = Fact
End Function Factorial


!---------------------------------------------------------------
Function StdNorm_PDF(Z) 
!---------------------------------------------------------------
 
	    DOUBLE PRECISION, parameter :: PI = 3.141592654
		DOUBLE PRECISION  Z
		StdNorm_PDF = 1.0 / (2.0 * PI) ** 0.5 * Exp(-Z ** 2 / 2.0)
End Function StdNorm_PDF

!---------------------------------------------------------------
Function StdNorm_CDF(Z)
!---------------------------------------------------------------
	    DOUBLE PRECISION, parameter :: SmallVal = 0.000000000000001
	    DOUBLE PRECISION  :: Z,vtemp

		If (Z < -4) Then
		  StdNorm_CDF = SmallVal
	    ElseIf (Z > 4) Then
		  StdNorm_CDF = 1.0 - SmallVal
	    Else
		  vtemp = Gauss_ERFC(Z / 2 ** 0.5)
		  StdNorm_CDF = 1.0 / 2.0 * (1.0 + vtemp)
	    End If
End Function StdNorm_CDF

!---------------------------------------------------------------
Function Gauss_ERFC(x)
!---------------------------------------------------------------
 
	    DOUBLE PRECISION, parameter :: crit = 0.0000001, PI = 3.141592654
		integer	N
		DOUBLE PRECISION ::	x, SumVar, Series, temp, temp2, Factorial

	    SumVar = 0.0
	    do N = 0 , 25
		  temp = ((-1.0) ** N * x ** (2.0 * N + 1.0)) 
		  temp2 = (2.0 * N + 1.0) * Factorial(N)
		  Series = temp / temp2
		  If (Abs(Series) > crit) Then
			  SumVar = SumVar + Series
		  Else
			  Exit
		  End If
	    enddo
	    Gauss_ERFC = 2.0 / (PI ** 0.5) * SumVar
End Function Gauss_ERFC

