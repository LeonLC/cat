# wrapper function  
Get_HazardVAL <- function(RtnPrd , ary_Ymean, ary_Yrate, ary_Count , sigma_lnY, distflag) {
  dyn.load('HazMap_Lib_X64.dll')
  out <- .Fortran('Get_HazVAL', as.integer(RtnPrd), as.double(ary_Ymean),as.double(ary_Yrate), as.integer(ary_Count), as.double(sigma_lnY), as.integer(distflag), haz=double(1))
  return(out$haz)
}