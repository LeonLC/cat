Get_HazardVAL <- function(RtnPrd, ary_Ymean, ary_Yrate, ary_Count, sigma_lnY, distflag ) {       
  
  #   RtnPrd = period
  #   ary_Ymean = geoID_data$PGA
  #   ary_Yrate = geoID_data$RATE
  #   distflag = 1     # distflag  !--- 1=LOGNORMAL, 2=NORMAL
  #   sigma_lnY = sigma
  
  ProbRes = 0.0000001
  HazRes = 0.0001
  
  #--- Search for corresponding hazard value using half-interval method ----
  
  if (distflag==1) {
    yHI = 5.0
    yLO = 0.00000001
  }
  if (distflag==2) {
    yHI = 12.0
    yLO = 0.00000001
  }
  
  
  if (distflag==1) {
    fyHI = sum(ary_Yrate*pnorm(log(yHI), log(ary_Ymean), sigma_lnY,lower.tail= F))
    fyLO = sum(ary_Yrate*pnorm(log(yLO), log(ary_Ymean), sigma_lnY,lower.tail= F))   
  }
  if (distflag==2) {
    fyHI = sum(ary_Yrate*pnorm(yHI, ary_Ymean, sigma_lnY,lower.tail= F))
    fyLO = sum(ary_Yrate*pnorm(yLO, ary_Ymean, sigma_lnY,lower.tail= F))       
  }
  
  #--- CHECK FOR EXTREME VALUES -----
  if (fyLO < 1/RtnPrd) {
    Get_HazVAL = yLO
  }
  
  if (fyHI > 1/RtnPrd) {
    Get_HazVAL = yHI
  }
  
  if ((fyLO > 1/RtnPrd) &  (1/RtnPrd > fyHI)) {
    Prob_err = 100000
    Haz_err = 100000   
    while ((Prob_err > ProbRes) & (Haz_err > HazRes))  { 
      yMD = (yHI + yLO) / 2
      if (distflag == 2) {  
        fyMD = sum(ary_Yrate*(1-pnorm(yMD, ary_Ymean, sigma_lnY)))        
      }
      if (distflag == 1) {  
        fyMD = sum(ary_Yrate*(1-pnorm(log(yMD), log(ary_Ymean), sigma_lnY)))        
      }
      Prob_err = abs(fyMD - (1 / RtnPrd))
      Haz_err = yHI - yLO
      if (fyMD > (1 / RtnPrd)) {
        yLO = yMD
        fyLO = fyMD
      }
      if (fyMD <= (1 / RtnPrd)){
        yHI = yMD
        fyHI = fyMD
      }
    }
    Get_HazVAL = yMD
  }
  return(Get_HazVAL)
}
