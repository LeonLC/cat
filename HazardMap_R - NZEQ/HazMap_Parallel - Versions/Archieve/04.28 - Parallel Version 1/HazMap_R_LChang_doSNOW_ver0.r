# read hazard/eventid/geoid for hazard map generation
# April 10, 2013, by Lchang

rm(list = ls())

setwd("C:\\Users\\lchang\\Desktop\\hazmap\\") #Set working dir

# =========================================================
# Rewrite the HazMap FORTRAN codes
# =========================================================

#--------------BEGIN Read DiscCalc Inputs
inFile = "HazMap.in"
con <- file(inFile, "rt", blocking = FALSE)

fnam_in1 = readLines(con, n=1)
#  fnam_in1 = "eventset.jpt"
cat(fnam_in1,'\n')

#     2. READ THE GEOCODE DATA FILENAME
fnam_in2 = readLines(con,n=1)
#	fnam_in2 = "geodata.jp"
cat(fnam_in2,'\n')

#     3. READ THE OUTPUT FILENAME FOR DISTANCE FOOTPRINT
fnam_out1= readLines(con,1)
#     4. READ THE OUTPUT FILENAME FOR LOG FILE
fnam_out2= readLines(con,1)
#     5. READ THE CRITICAL DISTANCE FOR OUTPUT FILTER
Crit_dist = as.numeric(readLines(con,1))
#     6. READ THE CRITICAL DISTANCE FOR OUTPUT FILTER
Pga_Thresh= as.numeric(readLines(con,1))
#     7. READ THE Pga_Thresh
#     read(9,*) MMI_Thresh

#Output hazard file
fname_out3 = readLines(con,1)

# list of SA periods
SA_period = as.numeric(strsplit(readLines(con,1),",")[[1]])  # always include 0 period for PGA
num_SA_period = length(SA_period)

#list of return period
RtnPrd = as.numeric(strsplit(readLines(con,1),",")[[1]])
Num_RP =length(RtnPrd)

close(con)

# END of reading input parameters

#=======================================
# c  --- HAZ_CALC ARGUMENT DEFINITIONS -------------------------
MaxAtten = 200
NumSource = 10000
MaxRupt = 500
MaxEvt = 100000
MaxGeocnt = 2000000

# c	--- EVENT SET FILE VARIABLES ------------------------------

Event_Data = data.frame(  EventID = integer(MaxEvt), 
                          region = character(MaxEvt),
                          SourceID = integer(MaxEvt),
                          Src_Name = character(MaxEvt),
                          Mag = double(MaxEvt),
                          SegID = integer(MaxEvt),
                          NumRupt = integer(MaxEvt),
                          Rate= double(MaxEvt),
                          EvtType = character(MaxEvt),
                          fAtten = integer(MaxEvt), stringsAsFactors = F)

fLong1 <- matrix(0.0, MaxEvt,MaxRupt)
fLat1  <- matrix(0.0, MaxEvt,MaxRupt)
fLong2 <- matrix(0.0, MaxEvt,MaxRupt)
fLat2  <- matrix(0.0, MaxEvt,MaxRupt)
fDepth <- matrix(0.0, MaxEvt,MaxRupt)

sigma = 0.56


# load("Event_Geo.RData")

#-----BEGIN READ DATA-------------------------------
#      READ INFO FROM GEOCODE DATA FILE
cat('     Reading GeoCode data: ', fnam_in2, "\n")
# 	--- READ HEADER LINE --------------------------
GeoCode_Data = read.csv(file= fnam_in2,head=TRUE,sep=",")
geocnt = nrow(GeoCode_Data)
names(GeoCode_Data) <- c("GeoID", "gLong","gLat","gSoil", "gLiq", "gSlide")
# GeoCode_Data$GeoID -> GeoID
# GeoCode_Data$GeoID -> gLong
# GeoCode_Data$GeoID -> gLat

#  READ INFO FROM EVENT SET FILE
#  fnam_in1 = "eventset.jpt"
cat('     Reading EventSet data: ', fnam_in1, "\n")
# read header file "eventhdr.nzt"
eventCnt <- nrow(read.table("eventhdr.nzt", header=F,sep="\n", as.is=T, nrow =1e8))
con <- file(fnam_in1, "rt", blocking = F)
i=0
# 
# headerLn = read.fwf(con,widths = c(7,3,5,41,7,4,4,13,3,5), n=1, colClasses=c("integer","character", "integer","character", "double", "integer", "integer","double", "character", "integer"))
# 
# if (length(headerLn)==0) {
#   EOF = TRUE
#   stop("No Event File to Read (end of file)")
# }

while (i<eventCnt) {
  headerLn <- read.fwf(con,widths = c(7,3,5,41,7,4,4,13,3,5), n=1, colClasses=c("integer","character", "integer","character", "double", "integer", "integer","double", "character", "integer") )
  # FORTRAN FORMAT  
  #   c  --- Event Set File Header and Body ----------------------- 
  #   c  changed to older format as the NZ fault file is old (2006)
  #   5701  format(i7,1x,a2,1x,i4,1x,a40,1x,f6.4,1x,i3,1x,i3,1x,f12.8,
  #               &	   1x,a2,1x,i4)
  #     c5701	format(i7,1x,a2,1x,i7,1x,a40,1x,f6.4,1x,i3,1x,i3,1x,e12.6,
  #                  c     &	   1x,a2,1x,i4,1x,f4.1)
  
  i=i+1
  headerLn -> Event_Data[i,]
  #   eventCnt = eventCnt + 1
  
  ruptureLn =   readLines(con,n=Event_Data[i,7])
  
  for (rup in 1: Event_Data[i,7]) {
    temp <-as.numeric(strsplit(gsub('^ *|(?<= ) | *$','',ruptureLn, perl=T)," ")[[rup]])  
    #Removing multiple spaces and trailing spaces using gsub
    fLong1[i,rup] <- temp[1] 
    fLat1[i,rup] <- temp[2] 
    fLong2[i,rup] <- temp[3] 
    fLat2[i,rup] <- temp[4] 
    fDepth[i,rup]  <- temp[5] 
  }  
  #   headerLn = read.fwf(con,widths = c(7,3,5,41,7,4,4,13,3,5), n=1, colClasses=c("integer","character", "integer","character", "double", "integer", "integer","double", "character", "integer"))
  #   if (length(headerLn)==0) {EOF = TRUE}
}
close(con)

#Trim Event type
library("gdata")
Event_Data$EvtType <- trim(Event_Data$EvtType)

# =============================================================

footcnt = 0

# disctance calculation function
source("disc.R")

# #load NZEQ attenuation funtion
# dyn.load("Dll5R_x64.dll", local=T, now=F)
# is.loaded("mcvsa")
# is.loaded("mcvpga")
# # source("d:/TEST/HazMap_LC/R/GMPE.R")

# Hazmap map function for return periods
source("Get_HazardVAL.R")

library(foreach)
library(doSNOW)

source("hazMapGen.R")

# check OUTPUT file exists and if yes, remove
if (file.exists(fname_out3)) {file.remove(fname_out3)} 
cat("GEOID,N,RP1,RP2,RP3",file=fname_out3,sep=",",append=T)
cat("\n",file=fname_out3, append=T)

cl = makeCluster(20, type = "SOCK")
registerDoSNOW(cl)
getDoParWorkers()
# getDoParName()
# getDoParVersion()

# Start the clock!
ptm <- proc.time()

max(Event_Data$NumRupt) -> MaxRupt

foreach (k = 1:geocnt, .combine = "rbind") %dopar% {
  cat("running...", k, "\n")
cat("+GeoID:  ")
out<- hazMapGen(MaxRupt, GeoCode_Data$GeoID[k],GeoCode_Data$gLong[k] ,GeoCode_Data$gLat[k], geocnt, eventCnt, Event_Data, fLong1, fLat1, fLong2, fLat2, Crit_dist, num_SA_period, fDepth, SA_period, Pga_Thresh,  Num_RP, RtnPrd, sigma)
cat(out,file=fname_out3,sep=",",append=T)
cat("\n",file=fname_out3, append=T)
} # end GEOID loop 
stopCluster(cl)
# Stop the clock
proc.time() - ptm

cat("Done!")

# # Unload DLL
# dyn.unload("Dll5R_x64.dll")

