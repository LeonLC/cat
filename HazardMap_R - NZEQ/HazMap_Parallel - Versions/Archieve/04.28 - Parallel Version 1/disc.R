# ***************************************************************************
#   *  Filename: IRAS_Lib.for
# *
#   *  This file contains subroutines ported from the IRAS 3.7 .CPP code
# *
#   *  >>> EQLOSS.cpp <<<
#   *  14.dist -- Function for calculating the distance between to points.
# ***************************************************************************
#   c 14. Function for calculating the distance between a line segment and a point
disc <- function(x1, y1, x2, y2, x0, y0) {
  
#   x1= 172.878
#   y1=  -41.797
#   x2 = 172.41
#   y2 = -42.058
#   x0 = 166.45
#   y0= -45.65
    
  # c	parameter (Ignorable = 1e-15, buffersize = 1e-5)
  buffersize = 1e-5
  Ignorable = 1e-15	
  pi=0.017453293
  
  # c	--- 3.2 fix; Bug #364.
  if ((abs(x1-x2) < Ignorable) & (abs(y1-y2) < Ignorable)) {
    
    # c		--- 3.2 fix; Bug #458. 12/14/94.
    d = sqrt(((x1-x0)*cos(pi*(y1+y0)/2))^2+(y1-y0)^2)*112.0
  } else {
    x = (x0*(x1-x2)^2+(x1-x2)*(y2-y1)*(y1-y0)+x1*(y2-y1)^2)/((x1-x2)^2+(y2-y1)^2)
    y = (y0*(y1-y2)^2+(y1-y2)*(x2-x1)*(x1-x0)+y1*(x2-x1)^2)/((y1-y2)^2+(x2-x1)^2)
    
    # c		--- Modified as per spec: sp40_059.doc GLM 02.18.00 ---
    xmax = max(x1, x2) + buffersize
    xmin = min(x1, x2) - buffersize
    
    # c		--- the changes are requested by Weimin Dong. 12/15/94.
    # c		--- Modified as per spec: sp40_059.doc GLM 02.18.00 ---
    ymax = max(y1, y2) + buffersize
    ymin = min(y1, y2) - buffersize
    if ((x > xmax) | (x < xmin) | (y > ymax) |	(y < ymin)) {
      
      d1 = sqrt(((x1-x0)*cos(pi*(y1+y0)/2))^2+(y1-y0)*(y1-y0))
      d2 = sqrt(((x2-x0)*cos(pi*(y2+y0)/2))^2+(y2-y0)*(y2-y0))
      d = min(d1,d2)*112.0
    } else {
      #   c			write(*,*) y,y0,pi
      d= 112.0*sqrt(((x-x0)*cos(pi*(y+y0)/2))^2+(y-y0)^2)
    }
  }

  return(d)
}