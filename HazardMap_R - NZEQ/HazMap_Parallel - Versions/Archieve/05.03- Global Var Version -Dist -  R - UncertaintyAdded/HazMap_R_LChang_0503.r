# read hazard/eventid/geoid for hazard map generation
# April 10, 2013, by Lchang

rm(list = ls())

mywd <- "C:\\Users\\lchang\\Desktop\\hazmap\\0503- Global Var Version -Dist -  R - UncertaintyAdded\\"
setwd(mywd) #Set working dir

# =========================================================
# Read Data for the HazMap
# =========================================================
source("Read_InputPara.R")
source("Read_DataFiles.R")

save.image(file = "AllEvent_1K_URG.RData")

load("AllEvent_1K_URG.RData")

cat("begin hazmap calculation... \n")
# =============================================================
# Hazmap map function for return periods
source("Get_HazardVAL.R"); source("hazMapGen.R")       
source("GeoID_Loop.R")

#Outut
# source("Output.R")