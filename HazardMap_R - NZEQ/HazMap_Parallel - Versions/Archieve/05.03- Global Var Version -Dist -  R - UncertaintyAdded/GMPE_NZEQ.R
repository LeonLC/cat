# wrapper ffunction for FORTRAN DLL

# dyn.load("Dll7F_x64.dll", local=T, now=F)
#   cat(is.loaded("mcvsa"))
#   cat(is.loaded("mcvpga"))


    # Mw = 6.0
    # eDist=10.0
    # h=10.0
    # ftype=1.0
    # T=1

# test = MCV06_SHALLOW_SA( Mw,  eDist,  h, ftype, T)
# test2 = MCV06_SHALLOW_PGA( Mw,  eDist,  h, ftype)
  
MCV06_SHALLOW_SA <- function(Mw,  eDist,  h, ftype, T) {
 dyn.load('DLL7F_x64.dll')
out <- .Fortran('MCV06_SHALLOW_SA', as.double(Mw), as.double(eDist),as.double(h), as.double(ftype), as.double(T), SA=double(1), sigma=double(1))
return(c(out$SA, out$sigma))
}
    
MCV06_SHALLOW_PGA <- function(Mw,  eDist,  h, ftype) {
  dyn.load('DLL7F_x64.dll')
  out <- .Fortran('MCV06_SHALLOW_PGA', as.double(Mw), as.double(eDist),as.double(h), as.double(ftype),PGA=double(1), sigma=double(1))
  return(c(out$PGA, out$sigma))
}
