hazMapGen <- function (GeoID,gLong ,gLat, geocnt) {
# hazMapGen <- function (MaxRupt,GeoID,gLong ,gLat, geocnt, eventCnt, Event_Data, fLong1, fLat1, fLong2, fLat2, Crit_dist, num_SA_period, fDepth, SA_period, Pga_Thresh,  Num_RP, RtnPrd, sigma) {
    
  
  #load NZEQ attenuation funtion
  dyn.load("Dll5R_x64.dll", local=T, now=F)
#   cat(is.loaded("mcvsa"))
#   cat(is.loaded("mcvpga"))
  
  # disctance calculation function
  source("dist.R")
  
  # return period hazard calcs
  source("Get_HazardVAL.R")
  
  Haz_Out <- array(0.0, length(num_SA_period))  # Hazard Array: May contain PGA, SA(T), etc.
  rHaz_Out<-matrix(0.0, 3, MaxRupt)
  HazVal <- array(0.0,4)  
  ary_Ypga <-array(0.0, eventCnt)
  ary_Yrate <-array(0.0, eventCnt)
  RuptDist <- double(MaxRupt)
  Rupt_Pass <- logical(MaxRupt)

  cat("+GeoID:  ", GeoID, "\n")
  
  # number of records for a geoid 
  icnt = 0  
  
  for (i in 1:eventCnt){
    
    #     c    	*************************************************
    #       c			APPLY OUTPUT FILTER HERE
    #     c			*************************************************
    MinDist = 10000000.0
    Num_OK  = 0
    
    NumRupt <- Event_Data$NumRupt[i]
    Rate <- Event_Data$Rate[i]
    Mag <- Event_Data$Mag[i]
    EvtType <- Event_Data$EvtType[i]
    
    for (r in 1:NumRupt ) {
      
      rHaz_Out[1:3,r] <- 0.0
      Rupt_Pass[r] = FALSE
      
      #Distance function to return RupDist
      RuptDist[r] <- dist(fLong1[i,r],fLat1[i,r],fLong2[i,r],fLat2[i,r],gLong,gLat)
      #       cat(RuptDist[r],'\n' )
      
      if (RuptDist[r] < Crit_dist) {
        
        #   set fault type = 0.0      ** LCHANG  
        for (ii in 1:num_SA_period) {
          if (ii==1) {  # PGA
            out <- .Fortran("mcvpga",as.double(Mag), as.double(RuptDist[r]), as.double(fDepth[i,r]), as.double(0.0),IM = double(1)) 
            Haz_Out[ii] <- out$IM  
            #             cat('PGA = ',Haz_Out[ii], '\n')
          } else {  
            out <- .Fortran("mcvsa",as.double(Mag), as.double(RuptDist[r]), as.double(fDepth[i,r]), as.double(0.0),as.double(SA_period[ii]),IM = double(1))  #SA 0.3s   
            Haz_Out[ii] <- out$IM         
          }          
        }
        
        if (Haz_Out[1] >= Pga_Thresh) {       
          for (h in  1: num_SA_period) {
            rHaz_Out[h,r] = Haz_Out[h]
          }
          Num_OK = Num_OK + 1
          Rupt_Pass[r] = TRUE
        }
      }
    }
    
    
    #*************************************************************
    #---  IF NO RUPTURE PASSES THE FILTER,THEN DON'T WRITE THE HEADER, MOVE TO NEXT GEOID	 ---
    if (Num_OK < 1) next	
    
    #---  GET THE RUPTURE INDEX, rMin, THAT IS CLOSEST TO THE  LOCATION											  ---
    #     call aMin(RuptDist,1,NumRupt(i),MinDist,rMin)
    MinDist = min(RuptDist[1: NumRupt])
    rMin = which.min(RuptDist[1: NumRupt])
    
    #--- CALL ATTENUATION EQTN AT THE MINIMUM DISTANCE TO GET THE CORRECT HAZARD TYPE FOR HEADER
    for (ii in 1:num_SA_period) {
      if (ii==1) {  # PGA
        out <- .Fortran("mcvpga",as.double(Mag), as.double(MinDist), as.double(fDepth[i,rMin]), as.double(0.0),IM = double(1)) 
        Haz_Out[ii] <- out$IM  
      } else {  
        out <- .Fortran("mcvsa",as.double(Mag), as.double(MinDist), as.double(fDepth[i,rMin]), as.double(0.0),as.double(SA_period[ii]),IM = double(1))  #SA s   
        Haz_Out[ii] <- out$IM         
      }          
    }    
    
    # FOR FAULT TYPE='LN' OR ='DF' (FULL RUPTURE DIPPING PLANE),	OR = 'D2', PROCESS THE RUPTURE CLOSEST TO THE LOCATION ONLY.

    if (EvtType == 'LN' | EvtType == 'ln' | EvtType=='d2' | EvtType == 'D2' | EvtType == 'DF') {
      
      DTOR = fDepth[i,1]
      DBOR = fDepth[i,NumRupt]
      
      #     c				--- PROCESS AND WRITE ONLY THE NEAREST RUPTURE FOR LN OR DF OR D2 ---
      #     c				--- WRITE THE EVENT-LOCATION HEADER AND DATA ---
      #       footcnt = footcnt + 1
      
      #   write(10,555) GeoID(k),EventID(i), Rate(i),MinDist,  Haz_Out(1),Haz_Out(2),Haz_Out(3)
      icnt = icnt + 1	
      ary_Ypga[icnt] = Haz_Out[1]
      ary_Yrate[icnt] = Rate
      
      # 	-----------------------------------------------------------  
    } else {
      #     c				--- FOR LN and D1 not fully ruptured
      #     C				--- WRITE THE EVENT-LOCATION HEADER -----------------------
      #       footcnt = footcnt + 1
      for (r in 1: NumRupt) {   
        if (Rupt_Pass[r]) {          
          DTOR = fDepth[i,r]
          DBOR = fDepth[i,r]
          #     !--- ADJUST RATE FOR MULTIPLE SUB-RUPTURES
          SubRate = Rate/NumRupt 
          for (ii in 1:num_SA_period) {
            if (ii==1) {  # PGA
              out <- .Fortran("mcvpga",as.double(Mag), as.double(RuptDist[r]), as.double(fDepth[i,r]), as.double(0.0),IM = double(1)) 
              Haz_Out[ii] <- out$IM  
            } else {  
              out <- .Fortran("mcvsa",as.double(Mag), as.double(RuptDist[r]), as.double(fDepth[i,r]), as.double(0.0),as.double(SA_period[ii]),IM = double(1))  #SA 0.3s   
              Haz_Out[ii] <- out$IM         
            }          
          }
          #   write(10,555) GeoID(k),EventID(i), SubRate,RuptDist(r),Haz_Out(1), Haz_Out(2),Haz_Out(3)
          icnt = icnt + 1	
          ary_Ypga[icnt] = Haz_Out[1]
          ary_Yrate[icnt] = SubRate 
        } 
      } 
    }
  }   
  # !--- END EVENT LOOP 
  #   Next	CALL HAZARD MAP  --- > Different Periods 
  
  HazVal[ 1] = GeoID       
  for (rp in 1: Num_RP) {
    HazVal[rp+1] = Get_HazardVAL(RtnPrd[rp],ary_Ypga[1:icnt],ary_Yrate[1:icnt], icnt, sigma,1)            
  }
  
#   cat(HazVal[ 1],icnt,HazVal[2:(Num_RP+1)],file=fname_out3,sep=",",append=T)
#   cat("\n",file=fname_out3, append=T)  
  
#   cat(HazVal[1],icnt,HazVal[2:(Num_RP+1)], sep="\t")
#   cat("\n")  
  
  # Unload DLL
 # dyn.unload("Dll5R_x64.dll")
  
  return(c(HazVal[ 1],icnt,HazVal[2:(Num_RP+1)]))
}