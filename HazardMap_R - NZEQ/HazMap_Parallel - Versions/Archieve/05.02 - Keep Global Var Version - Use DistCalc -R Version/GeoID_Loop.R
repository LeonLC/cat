library(foreach);     library(doSNOW)

ptm <- proc.time()  # Start the clock

if (numWorker > 1) { # parallel processing
  cl = makeCluster(numWorker, type = "SOCK") ; registerDoSNOW(cl)
  cat(getDoParWorkers(), "workers created...\n")
  
  # GEOID loop 
  out<- foreach(k=1:geocnt,.combine = "rbind") %dopar% {hazMapGen(GeoCode_Data$GeoID[k],GeoCode_Data$gLong[k],GeoCode_Data$gLat[k])} 
  stopCluster(cl)
} else {   # sequential processing
  cat("Sequential processing...\n")
  # GEOID loop 
  out<- foreach(k=1:geocnt,.combine = "rbind") %do% {hazMapGen(GeoCode_Data$GeoID[k],GeoCode_Data$gLong[k],GeoCode_Data$gLat[k])} 
}

timeCost <- (proc.time() - ptm)[[3]] # Stop the clock
cat("Calculation done in", timeCost, "(seconds) \n")