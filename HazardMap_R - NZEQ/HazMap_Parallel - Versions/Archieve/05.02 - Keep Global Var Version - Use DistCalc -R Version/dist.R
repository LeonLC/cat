dist <- function (x1,y1,x2,y2,x0,y0) {
  dyn.load('dist_x64.dll')
  out <- .Fortran('dist', as.double(x1), as.double(y1),as.double(x2), as.double(y2), as.double(x0), as.double(y0), d=double(1))
  return(out$d)
}