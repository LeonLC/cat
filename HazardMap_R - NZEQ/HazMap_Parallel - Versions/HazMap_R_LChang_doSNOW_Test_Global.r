# read hazard/eventid/geoid for hazard map generation
# April 10, 2013, by Lchang

rm(list = ls())

setwd("C:\\Users\\lchang\\Desktop\\hazmap\\") #Set working dir

# =========================================================
# Rewrite the HazMap FORTRAN codes
# =========================================================

#--------------BEGIN Read DiscCalc Inputs
inFile = "HazMap.in"
con <- file(inFile, "rt", blocking = FALSE)

fnam_in1 = readLines(con, n=1)
#  fnam_in1 = "eventset.jpt"

#     READ THE OUTPUT FILENAME FOR DISTANCE FOOTPRINT
fnam_in2= readLines(con,1)   # fnam_in2 used as the flt hdr file

#     READ THE GEOCODE DATA FILENAME
fnam_in3 = readLines(con,n=1)
#	fnam_in3 = "geodata.jp"

#     READ THE OUTPUT FILENAME FOR LOG FILE
fnam_out2= readLines(con,1)

#     READ THE CRITICAL DISTANCE FOR OUTPUT FILTER
Crit_dist <<- as.numeric(readLines(con,1))

#     READ THE Pga_Thresh
Pga_Thresh <<- as.numeric(readLines(con,1))

#Output hazard file
fname_out3 = readLines(con,1)

# list of SA periods
SA_period <<- as.numeric(strsplit(readLines(con,1),",")[[1]])  # always include 0 period for PGA
num_SA_period <<- length(SA_period)

#list of return period
RtnPrd <<- as.numeric(strsplit(readLines(con,1),",")[[1]])
Num_RP <<- length(RtnPrd)

#number of workers
numWorker <- as.numeric(strsplit(readLines(con,1),",")[[1]])
close(con)

# END of reading input parameters

#=======================================
# c  --- HAZ_CALC ARGUMENT DEFINITIONS -------------------------
MaxAtten = 200
NumSource = 10000
MaxRupt = 500
MaxEvt = 100000
MaxGeocnt = 2000000

# c	--- EVENT SET FILE VARIABLES ------------------------------

Event_Data = data.frame(  EventID = integer(MaxEvt), 
                          region = character(MaxEvt),
                          SourceID = integer(MaxEvt),
                          Src_Name = character(MaxEvt),
                          Mag = double(MaxEvt),
                          SegID = integer(MaxEvt),
                          NumRupt = integer(MaxEvt),
                          Rate= double(MaxEvt),
                          EvtType = character(MaxEvt),
                          fAtten = integer(MaxEvt), stringsAsFactors = F)

fLong1 <- matrix(0.0, MaxEvt,MaxRupt)
fLat1  <- matrix(0.0, MaxEvt,MaxRupt)
fLong2 <- matrix(0.0, MaxEvt,MaxRupt)
fLat2  <- matrix(0.0, MaxEvt,MaxRupt)
fDepth <- matrix(0.0, MaxEvt,MaxRupt)

sigma <<- 0.56


# load("Event_Geo.RData")

#-----BEGIN READ DATA-------------------------------
#      READ INFO FROM GEOCODE DATA FILE
cat('     Reading GeoCode data: ', fnam_in3, "\n")
# 	--- READ HEADER LINE --------------------------
GeoCode_Data = read.csv(file= fnam_in3,head=TRUE,sep=",")
geocnt <<- nrow(GeoCode_Data)
names(GeoCode_Data) <- c("GeoID", "gLong","gLat","gSoil", "gLiq", "gSlide")

#  READ INFO FROM EVENT SET FILE
#  fnam_in1 = "eventset.jpt"
cat('     Reading EventSet data: ', fnam_in1, "\n")
# read header file "eventhdr.nzt"
eventCnt <<- nrow(read.table(fnam_in2, header=F,sep="\n", as.is=T, nrow =1e8))
con <- file(fnam_in1, "rt", blocking = F)
i=0

# headerLn = read.fwf(con,widths = c(7,3,5,41,7,4,4,13,3,5), n=1, colClasses=c("integer","character", "integer","character", "double", "integer", "integer","double", "character", "integer"))
# 
# if (length(headerLn)==0) {
#   EOF = TRUE
#   stop("No Event File to Read (end of file)")
# }

while (i<eventCnt) {
  headerLn <- read.fwf(con,widths = c(7,3,5,41,7,4,4,13,3,5), n=1, colClasses=c("integer","character", "integer","character", "double", "integer", "integer","double", "character", "integer") )
  # FORTRAN FORMAT  
  #   c  --- Event Set File Header and Body ----------------------- 
  #   c  changed to older format as the NZ fault file is old (2006)
  #   5701  format(i7,1x,a2,1x,i4,1x,a40,1x,f6.4,1x,i3,1x,i3,1x,f12.8,
  #               &	   1x,a2,1x,i4)
  #     c5701	format(i7,1x,a2,1x,i7,1x,a40,1x,f6.4,1x,i3,1x,i3,1x,e12.6,
  #                  c     &	   1x,a2,1x,i4,1x,f4.1)
  
  i=i+1
  headerLn -> Event_Data[i,]
  #   eventCnt = eventCnt + 1
  
  ruptureLn =   readLines(con,n=Event_Data[i,7])
  
  for (rup in 1: Event_Data[i,7]) {
    temp <-as.numeric(strsplit(gsub('^ *|(?<= ) | *$','',ruptureLn, perl=T)," ")[[rup]])  
    #Removing multiple spaces and trailing spaces using gsub
    fLong1[i,rup] <- temp[1] 
    fLat1[i,rup] <- temp[2] 
    fLong2[i,rup] <- temp[3] 
    fLat2[i,rup] <- temp[4] 
    fDepth[i,rup]  <- temp[5] 
  }  
  #   headerLn = read.fwf(con,widths = c(7,3,5,41,7,4,4,13,3,5), n=1, colClasses=c("integer","character", "integer","character", "double", "integer", "integer","double", "character", "integer"))
  #   if (length(headerLn)==0) {EOF = TRUE}
}
close(con)

#Trim Event type
library("gdata")
Event_Data$EvtType <- trim(Event_Data$EvtType)

# Sve as global variables
Event_Data <<- Event_Data
MaxRupt <<- max(Event_Data$NumRupt)
fLong1 <<- fLong1 
fLat1 <<- fLat1
fLong2 <<- fLong2 
fLat2 <<- fLat2
fDepth  <<- fDepth 

cat("begin hazmap calculation... \n")
# =============================================================
# Hazmap map function for return periods
source("Get_HazardVAL.R"); source("hazMapGen.R")       

library(foreach);     library(doSNOW)

ptm <- proc.time()  # Start the clock

if (numWorker > 1) { # parallel processing
cl = makeCluster(numWorker, type = "SOCK") ; registerDoSNOW(cl)
cat(getDoParWorkers(), "workers created...\n")

# GEOID loop 
out<- foreach(k=1:geocnt,.combine = "rbind") %dopar% {hazMapGen(GeoCode_Data$GeoID[k],GeoCode_Data$gLong[k],GeoCode_Data$gLat[k])} 
stopCluster(cl)
} else {   # sequential processing
  cat("Sequential processing...\n")
  # GEOID loop 
  out<- foreach(k=1:geocnt,.combine = "rbind") %do% {hazMapGen(GeoCode_Data$GeoID[k],GeoCode_Data$gLong[k],GeoCode_Data$gLat[k])} 
}

timeCost <- (proc.time() - ptm)[[3]] # Stop the clock
cat("Calculation done in", timeCost, "(seconds) \n")

#Outut
if (file.exists(fname_out3)) {file.remove(fname_out3)}  # check OUTPUT file exists 
cat("GEOID","N",paste("RP", RtnPrd, sep=""),file=fname_out3,sep=",",append=T)
cat("\n",file=fname_out3,append=T)
write.table(out, fname_out3,sep=",", eol="\n", row.names=F, col.names=F, append = T)
cat("Output file ready! " )