# read hazard/eventid/geoid for hazard map generation
# April 10, 2013, by Lchang

rm(list = ls())

setwd("C:\\Users\\lchang\\Desktop\\hazmap\\") #Set working dir

# =========================================================
# Rewrite the HazMap FORTRAN codes
# =========================================================

#--------------BEGIN Read DiscCalc Inputs
inFile = "HazMap.in"
con <- file(inFile, "rt", blocking = FALSE)

fnam_in1 = readLines(con, n=1)
#  fnam_in1 = "eventset.jpt"
cat(fnam_in1,'\n')

#     2. READ THE GEOCODE DATA FILENAME
fnam_in2 = readLines(con,n=1)
#	fnam_in2 = "geodata.jp"
cat(fnam_in2,'\n')

#     3. READ THE OUTPUT FILENAME FOR DISTANCE FOOTPRINT
fnam_out1= readLines(con,1)
#     4. READ THE OUTPUT FILENAME FOR LOG FILE
fnam_out2= readLines(con,1)
#     5. READ THE CRITICAL DISTANCE FOR OUTPUT FILTER
Crit_dist = as.numeric(readLines(con,1))
#     6. READ THE CRITICAL DISTANCE FOR OUTPUT FILTER
Pga_Thresh= as.numeric(readLines(con,1))
#     7. READ THE Pga_Thresh
#     read(9,*) MMI_Thresh

#Output hazard file
fname_out3 = readLines(con,1)

# list of SA periods
SA_period = as.numeric(strsplit(readLines(con,1),",")[[1]])  # always include 0 period for PGA
num_SA_period = length(SA_period)

#list of return period
RtnPrd = as.numeric(strsplit(readLines(con,1),",")[[1]])
Num_RP =length(RtnPrd)

close(con)


# check OUTPUT file exists and if yes, remove
if (file.exists(fname_out3)) {
  file.remove(fname_out3)
} 

# END of reading input parameters

#=======================================
# c  --- HAZ_CALC ARGUMENT DEFINITIONS -------------------------
MaxAtten = 200
NumSource = 10000
MaxRupt = 500
MaxEvt = 100000
MaxGeocnt = 2000000

# c	--- EVENT SET FILE VARIABLES ------------------------------

Event_Data = data.frame(  EventID = integer(MaxEvt), 
                          region = character(MaxEvt),
                          SourceID = integer(MaxEvt),
                          Src_Name = character(MaxEvt),
                          Mag = double(MaxEvt),
                          SegID = integer(MaxEvt),
                          NumRupt = integer(MaxEvt),
                          Rate= double(MaxEvt),
                          EvtType = character(MaxEvt),
                          fAtten = integer(MaxEvt), stringsAsFactors = F)

fLong1 <- matrix(0.0, MaxEvt,MaxRupt)
fLat1  <- matrix(0.0, MaxEvt,MaxRupt)
fLong2 <- matrix(0.0, MaxEvt,MaxRupt)
fLat2  <- matrix(0.0, MaxEvt,MaxRupt)
fDepth <- matrix(0.0, MaxEvt,MaxRupt)
# 
# # c  --- HAZARD DATA VARIABLES ---------------------------
Haz_Out <- array(0.0, length(num_SA_period))	# Hazard Array: May contain PGA, SA(T), etc.
# 
# c  --- OTHER PROGRAM VARIABLES --------------------------------
#   
#   dimension AttenMap(MaxAtten)		! Attenuation No. local to Global
# &		  ,ConvMap(MaxAtten)		! Pga to MMI Conversion no.
# &		  ,SoilShift(5,Maxatten)	! Soil Shift Array data	
RuptDist <- double(MaxRupt)
Rupt_Pass <- logical(MaxRupt)
rHaz_Out<-matrix(0.0, 3, MaxRupt)
# 
sigma = 0.56
# 
HazVal <- matrix(0.0,MaxGeocnt,4)  
# 
ary_Ypga <-array(0.0, MaxEvt)
ary_Yrate <-array(0.0, MaxEvt)


# load("Event_Geo.RData")

#-----BEGIN READ DATA-------------------------------
#      READ INFO FROM GEOCODE DATA FILE
cat('     Reading GeoCode data: ', fnam_in2, "\n")
# 	--- READ HEADER LINE --------------------------
GeoCode_Data = read.csv(file= fnam_in2,head=TRUE,sep=",")
geocnt = nrow(GeoCode_Data)
names(GeoCode_Data) <- c("GeoID", "gLong","gLat","gSoil", "gLiq", "gSlide")
# GeoCode_Data$GeoID -> GeoID
# GeoCode_Data$GeoID -> gLong
# GeoCode_Data$GeoID -> gLat

#  READ INFO FROM EVENT SET FILE
#  fnam_in1 = "eventset.jpt"
cat('     Reading EventSet data: ', fnam_in1, "\n")
# read header file "eventhdr.nzt"
eventCnt <- nrow(read.table("eventhdr.nzt", header=F,sep="\n", as.is=T, nrow =1e8))
con <- file(fnam_in1, "rt", blocking = F)
i=0
# 
# headerLn = read.fwf(con,widths = c(7,3,5,41,7,4,4,13,3,5), n=1, colClasses=c("integer","character", "integer","character", "double", "integer", "integer","double", "character", "integer"))
# 
# if (length(headerLn)==0) {
#   EOF = TRUE
#   stop("No Event File to Read (end of file)")
# }

while (i<eventCnt) {
  headerLn <- read.fwf(con,widths = c(7,3,5,41,7,4,4,13,3,5), n=1, colClasses=c("integer","character", "integer","character", "double", "integer", "integer","double", "character", "integer") )
  # FORTRAN FORMAT  
  #   c  --- Event Set File Header and Body ----------------------- 
  #   c  changed to older format as the NZ fault file is old (2006)
  #   5701  format(i7,1x,a2,1x,i4,1x,a40,1x,f6.4,1x,i3,1x,i3,1x,f12.8,
  #               &	   1x,a2,1x,i4)
  #     c5701	format(i7,1x,a2,1x,i7,1x,a40,1x,f6.4,1x,i3,1x,i3,1x,e12.6,
  #                  c     &	   1x,a2,1x,i4,1x,f4.1)
  
  i=i+1
  headerLn -> Event_Data[i,]
  #   eventCnt = eventCnt + 1
  
  ruptureLn =   readLines(con,n=Event_Data[i,7])
  
  for (rup in 1: Event_Data[i,7]) {
    temp <-as.numeric(strsplit(gsub('^ *|(?<= ) | *$','',ruptureLn, perl=T)," ")[[rup]])  
    #Removing multiple spaces and trailing spaces using gsub
    fLong1[i,rup] <- temp[1] 
    fLat1[i,rup] <- temp[2] 
    fLong2[i,rup] <- temp[3] 
    fLat2[i,rup] <- temp[4] 
    fDepth[i,rup]  <- temp[5] 
  }  
  #   headerLn = read.fwf(con,widths = c(7,3,5,41,7,4,4,13,3,5), n=1, colClasses=c("integer","character", "integer","character", "double", "integer", "integer","double", "character", "integer"))
  #   if (length(headerLn)==0) {EOF = TRUE}
}
close(con)

footcnt = 0

# disctance calculation function
source("disc.R")

#load NZEQ attenuation funtion
dyn.load("Dll5R_x64.dll", local=T, now=F)
is.loaded("mcvsa")
is.loaded("mcvpga")
# source("d:/TEST/HazMap_LC/R/GMPE.R")

# Hazmap map function for return periods
source("Get_HazardVAL.R")

library("gdata")

# ======================# ======================
#--- PROCESS ONE LOCATION AT A TIME ---
# ======================# ======================

for (k in 1: geocnt){
  cat("+", GeoCode_Data$GeoID[k], "     ", k, "/", geocnt,"\n")
  
  # number of records for a geoid 
  icnt = 0  
  
  for (i in 1:eventCnt){
    
    #     c  		*************************************************
    #       c			APPLY OUTPUT FILTER HERE
    #     c			*************************************************
    MinDist = 10000000.0
    Num_OK  = 0
    
    for (r in 1: Event_Data[i,7]) {
      
      rHaz_Out[1:3,r] <- 0.0
      Rupt_Pass[r] = FALSE
      
      #Distance function to return RupDist
      RuptDist[r] <- disc(fLong1[i,r],fLat1[i,r],fLong2[i,r],fLat2[i,r],GeoCode_Data$gLong[k],GeoCode_Data$gLat[k])
#       cat(RuptDist[r],'\n' )
      
      if (RuptDist[r] < Crit_dist) {
        
        #   set fault type = 0.0      ** LCHANG  
        for (ii in 1:num_SA_period) {
          if (ii==1) {  # PGA
            out <- .Fortran("mcvpga",as.double(Event_Data$Mag[i]), as.double(RuptDist[r]), as.double(fDepth[i,r]), as.double(0.0),IM = double(1)) 
            Haz_Out[ii] <- out$IM  
#             cat('PGA = ',Haz_Out[ii], '\n')
          } else {  
            out <- .Fortran("mcvsa",as.double(Event_Data$Mag[i]), as.double(RuptDist[r]), as.double(fDepth[i,r]), as.double(0.0),as.double(SA_period[ii]),IM = double(1))  #SA 0.3s   
            Haz_Out[ii] <- out$IM         
          }          
        }
        
        if (Haz_Out[1] >= Pga_Thresh) {       
          for (h in  1: num_SA_period) {
            rHaz_Out[h,r] = Haz_Out[h]
          }
          Num_OK = Num_OK + 1
          Rupt_Pass[r] = TRUE
        }
      }
    }
    
    
    #*************************************************************
    #---  IF NO RUPTURE PASSES THE FILTER,THEN DON'T WRITE THE HEADER, MOVE TO NEXT GEOID	 ---
    if (Num_OK < 1) next	
    
    #---  GET THE RUPTURE INDEX, rMin, THAT IS CLOSEST TO THE  LOCATION											  ---
    #     call aMin(RuptDist,1,NumRupt(i),MinDist,rMin)
    MinDist = min(RuptDist[1: Event_Data$NumRupt[i]])
    rMin = which.min(RuptDist[1: Event_Data$NumRupt[i]])
    
    #--- CALL ATTENUATION EQTN AT THE MINIMUM DISTANCE TO GET THE CORRECT HAZARD TYPE FOR HEADER
    for (ii in 1:num_SA_period) {
      if (ii==1) {  # PGA
        out <- .Fortran("mcvpga",as.double(Event_Data$Mag[i]), as.double(MinDist), as.double(fDepth[i,rMin]), as.double(0.0),IM = double(1)) 
        Haz_Out[ii] <- out$IM  
      } else {  
        out <- .Fortran("mcvsa",as.double(Event_Data$Mag[i]), as.double(MinDist), as.double(fDepth[i,rMin]), as.double(0.0),as.double(SA_period[ii]),IM = double(1))  #SA s   
        Haz_Out[ii] <- out$IM         
      }          
    }    
    
    # FOR FAULT TYPE='LN' OR ='DF' (FULL RUPTURE DIPPING PLANE),	OR = 'D2', PROCESS THE RUPTURE CLOSEST TO THE LOCATION ONLY.
    EvtType <- trim(Event_Data$EvtType[i])
    if (EvtType == 'LN' | EvtType == 'ln' | EvtType=='d2' | EvtType == 'D2' | EvtType == 'DF') {
      
      DTOR = fDepth[i,1]
      DBOR = fDepth[i,Event_Data$NumRupt[i]]
      
      #     c				--- PROCESS AND WRITE ONLY THE NEAREST RUPTURE FOR LN OR DF OR D2 ---
      #     c				--- WRITE THE EVENT-LOCATION HEADER AND DATA ---
      footcnt = footcnt + 1
      
      #   write(10,555) GeoID(k),EventID(i), Rate(i),MinDist,  Haz_Out(1),Haz_Out(2),Haz_Out(3)
      icnt = icnt + 1	
      ary_Ypga[icnt] = Haz_Out[1]
      ary_Yrate[icnt] = Event_Data$Rate[i]
      
      # 	-----------------------------------------------------------  
    } else {
      #     c				--- FOR LN and D1 not fully ruptured
      #     C				--- WRITE THE EVENT-LOCATION HEADER -----------------------
      footcnt = footcnt + 1
      for (r in 1: Event_Data$NumRupt[i]) {   
        if (Rupt_Pass[r]) {          
          DTOR = fDepth[i,r]
          DBOR = fDepth[i,r]
          #     !--- ADJUST RATE FOR MULTIPLE SUB-RUPTURES
          SubRate = Event_Data$Rate[i]/Event_Data$NumRupt[i]  
          for (ii in 1:num_SA_period) {
            if (ii==1) {  # PGA
              out <- .Fortran("mcvpga",as.double(Event_Data$Mag[i]), as.double(RuptDist[r]), as.double(fDepth[i,r]), as.double(0.0),IM = double(1)) 
              Haz_Out[ii] <- out$IM  
            } else {  
              out <- .Fortran("mcvsa",as.double(Event_Data$Mag[i]), as.double(RuptDist[r]), as.double(fDepth[i,r]), as.double(0.0),as.double(SA_period[ii]),IM = double(1))  #SA 0.3s   
              Haz_Out[ii] <- out$IM         
            }          
          }
          #   write(10,555) GeoID(k),EventID(i), SubRate,RuptDist(r),Haz_Out(1), Haz_Out(2),Haz_Out(3)
          icnt = icnt + 1	
          ary_Ypga[icnt] = Haz_Out[1]
          ary_Yrate[icnt] = SubRate 
        } 
      } 
    }
  }   
  # !--- END EVENT LOOP 
  #   Next	CALL HAZARD MAP  --- > Different Periods 
  
  HazVal[k, 1] = GeoCode_Data$GeoID[k]       
  for (rp in 1: Num_RP) {
    HazVal[k,rp+1] = Get_HazardVAL(RtnPrd[rp],ary_Ypga[1:icnt],ary_Yrate[1:icnt], icnt, sigma,1)            
  }
  # c             write(stdout,6000) GeoID(k),icnt,(HazVal(k,rp+1),rp=1,Num_RP)
  #              write(22,6000) GeoID(k),icnt,(HazVal(k,rp+1),rp=1,Num_RP)
  
  cat(HazVal[k, 1],icnt,HazVal[k,2:(Num_RP+1)],file=fname_out3,sep=",",append=T)
  cat("\n",file=fname_out3, append=T)  
  
  #             cat('GeoID Progress = ', k/geocnt*100, '\n')
  #   cat('\n')
  
} # end GEOID loop 

cat("Done!")
# Unload DLL
dyn.unload("Dll5R_x64.dll")
