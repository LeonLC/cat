
# library("sqldf")
# library("data.table")
# geoid_hazard <- scan("NZDistMap.out", skip = 1, quiet= TRUE)

# =======================================
# Read Tempary OUTPUT file of DiscCalc (fault, GM) and do the return period hazard calcs 
# =======================================
#  f <- file("TEST.txt")
f <- file("NZDistMap.out")
uniqueGeoID <- sqldf("select distinct GeoID from f", dbname = tempfile(), file.format = list(header = T, row.names = F))

inFille <- sqldf("select * from f", dbname = tempfile(), file.format = list(header = T, row.names = F))

# inFille  <- read.csv.sql("NZDistMap.out", sql = "select * from file", header=T, sep=",")

in_Hazard = 2 ##--- 1=PGA, 2=SA, 3=MMI ---
sigma = 0.56
numperiod = 3
out_filebase = "PGA_HazMap"
RtnPrd = c(475,1000,2500)

#  --- CHECK INPUT VALUE FOR SIGMA ---
if (sigma<1e-6) {
  if (in_Hazard == 1) {
    sigma =  sigma_lnPGA
  }
  if (in_Hazard == 2) {
    sigma =  sigma_lnSA
  }
  if (in_Hazard == 3) {
    sigma =  sigma_MMI
  }
  else
    sigma = 0.7
}

dt <- data.table(inFille)

uniqueGeoID <- unique(dt$GeoID)

numGeoID = length(uniqueGeoID)

HazVAL<-mat.or.vec(numGeoID,length(RtnPrd))

numRec = nrow(inFille)

j = 1

for (geoID in uniqueGeoID) {
  
  geoID <- 8664546
  geoID_data <-dt[dt$GeoID == geoID]
  
  #   geoID_data <- sqldf(sql_cmd, dbname = tempfile(), file.format = list(header = T, row.names = F))
  N = nrow(geoID_data)
  i = 1
  for (period in RtnPrd){           
    HazVAL[j,i] <- Get_HazardVAL(period, geoID_data$PGA,geoID_data$RATE, N, sigma,1)
    i=i+1 
  }
  cat(geoID,N,HazVAL[j,],file="data1.txt",sep=",",append=T)
  cat("\n",file="data1.txt",append=T)
  
  cat(geoID,geoid2latlong(geoID),N,HazVAL[j,],file="data2.txt",sep=",",append=T)
  cat("\n",file="data2.txt",append=T)  
  
  cat('Progress = ', j/numGeoID*100, '\n')
  cat('\n')
  
  j = j+1
}
cat('Program finished#')
cat("Number of Locations found =  ", numGeoID)
cat('Number of Records found = ', numRec)