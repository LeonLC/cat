﻿subroutine MCVSAP(Mw, eDist,  h, ftype, CC,Ci, SAp)

double precision  CC(22),  C(46), Mw, eDist, h, ftype, SAp
integer Ci(22)

!  !=======================================================
!  ! Function MCVERRY_ETAL_06_SHALLOW_SAp(ByVal Mw, ByVal eDist, ByVal h, ftype, CC, Ci)
!  !   McVerry, Zhao, Abrahamson, and Sommerville (2006) - uses Mw
!  !   ftype: 0=Unspecified, 1=SS, 2=REV, 3=IF, 4=IS, 5=NRML, 6=TRST, 7=OBLQ
!  !   eDist = closest dist to surface projection
!  !   NEHRP Soil Class ! (Vs=760 m/s)
!  !   Eqtn Source:
!  !   ---------------------------------------------------
!
!  !---  REINDEX THE COEFFICIENTS BASED ON VECTOR, Ci ---
!  
do i = 1,22 
C(Ci(i)) = CC(i)
end do

!---  SET COEFFICIENTS BASED ON FAULT TYPE ---
Select Case (int(ftype))
case (0 ,1)   !--- Default / Strike-Slip Fault
Cn = 0.0000
CR = 0.0000
Case (2)  !! Reverse Fault
Cn = 0.0000
CR = 1.0000
Case (5)  !! Normal Fault
Cn = -1.000
CR = 0.0000
case (7)  !! Oblique Fault
Cn = 0.0000
CR = 0.5000
end select  

rr = sqrt(h ** 2 + eDist ** 2)

R2 = C(5) * rr + (C(8) + C(6) * (Mw - 6)) * log((rr ** 2 + C(10) ** 2) ** 0.5)
w = C(1) + C(4) * (Mw - 6) + C(3) * (8.5 - Mw) ** 2 + R2 + C(32) * Cn + C(33) * CR
SAp = exp(w)

return
End subroutine MCVSAP


subroutine MCVPGA( Mw,  eDist,  h, ftype, PGA)
!DEC$ ATTRIBUTES DLLEXPORT::MCVPGA

double precision  CC(22),Mw, eDist, h, ftype, PGA
integer Ci(22)

!!    InitArray("pMcVerry_06", CC) !--- USES COEFFICIENT FOR PGA
CC = (/ 0.1427, 0.0000, -0.1440, -0.0099, 0.1700, -0.6874, 5.6000, 8.5734, 1.4140, & 
0.0000, -2.5520, -2.5659, 1.7818, 0.5540, 0.0155, -0.4996, 0.2732, -0.2300, 0.2000, 0.2600, -0.3372, -0.0326 /)
!!    InitArray("cMcVerry_06", Ci)
Ci = (/ 1,3,4,5,6,8,10,11,12,13,15,17,18,19,20,24,29,30,32,33,43,46 /)

call MCVSAP(Mw, eDist, h, ftype, CC, Ci, PGA)
return
end subroutine MCVPGA

!=======================================================
subroutine MCVPGAP( Mw,  eDist,  h, ftype, PGAp)

double precision  CC(22), Mw, eDist, h, ftype, PGAp
integer Ci(22)

!Call InitArray("qMcVerry_06", CC) !--- USES COEFFICIENT FOR PGAp
CC = (/0.0771,0.0000,-0.1440,-0.0090,0.1700,-0.7373,5.6000,8.0861,1.4140,0.0000, & 
-2.5520,-2.4989,1.7818,0.5540,0.0159,-0.4322,0.3873,-0.2300,0.2000,0.2600,-0.3104,-0.0325/)
!!    InitArray("cMcVerry_06", Ci)
Ci = (/ 1,3,4,5,6,8,10,11,12,13,15,17,18,19,20,24,29,30,32,33,43,46 /)

call MCVSAP(Mw, eDist, h, ftype, CC, Ci, PGAp)
return
End subroutine MCVPGAP

!=======================================================
subroutine MCVSA( Mw,  eDist,  h, ftype, T, SA)
!   McVerry, Zhao, Abrahamson, and Sommerville (2006) - uses Mw
!   ftype: 0=Unspecified, 1=SS, 2=REV, 3=IF, 4=IS, 5=NRML, 6=TRST, 7=OBLQ
!   eDist = closest dist to surface projection
!   NEHRP Soil Class ' (Vs=760 m/s)
!   Eqtn Source:
!   ---------------------------------------------------

!DEC$ ATTRIBUTES DLLEXPORT::MCVSA

double precision  CC(22), Mw, eDist, h, ftype, T, SA,SAp,PGA,PGAp 
integer Ci(22)

if (T ==0.3) THEN
! 0.3 sec ONLY 
CC = (/0.2112,-0.0360,-0.1440,-0.0103,0.1700,-0.5240,4.8000,9.2178,1.4140, & 
-0.0036,-2.4540,-2.4736,1.7818,0.5540,0.0130,-0.5660,0.5321,-0.1950,0.2000,0.1980,-0.0757,-0.0354/)
ELSE IF (T == 1.0) THEN
! 1 sec ONLY
CC = (/-0.5140,-0.1020,-0.1440,-0.0065,0.1700,-0.5867,3.7000,6.9874,1.4140, &
-0.0064,-2.2340,-2.2826,1.7818,0.5540,0.0093,0.0201,0.3300,0.0000,0.2000,0.0130,0.8924,-0.0250/)
ELSE IF (T == 3.0) THEN
! 3 sec ONLY
CC = (/-1.5657,-0.1726,-0.1440,-0.0062,0.1700,-0.5226,3.5000,5.0542,1.4140, & 
 -0.0089,-2.0330,-2.0556,1.7818,0.5540,-0.0027,-0.2397,0.0987,0.0400,0.2000,-0.1560,0.6094,-0.0159/)
END IF
!!    InitArray("cMcVerry_06", Ci)
Ci = (/ 1,3,4,5,6,8,10,11,12,13,15,17,18,19,20,24,29,30,32,33,43,46 /)

call MCVSAP(Mw, eDist, h, ftype, CC, Ci,SAp)
call MCVPGA(Mw, eDist, h, ftype, PGA)
call MCVPGAP(Mw, eDist, h, ftype, PGAp)

SA = SAp * (PGA / PGAp)

return
end subroutine MCVSA
